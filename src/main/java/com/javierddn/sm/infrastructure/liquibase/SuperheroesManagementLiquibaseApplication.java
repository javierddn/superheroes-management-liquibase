package com.javierddn.sm.infrastructure.liquibase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperheroesManagementLiquibaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuperheroesManagementLiquibaseApplication.class, args);
    }

}
